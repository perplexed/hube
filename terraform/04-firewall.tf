resource "hcloud_firewall" "k8s-firewall" {
  name = "k8s-firewall"
  rule {
    direction = "in"
    protocol  = "tcp"
    port      = 22
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }
}
