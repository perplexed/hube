resource "hcloud_server" "node" {
  count       = 3
  name        = "node${count.index}"
  image       = "ubuntu-20.04"
  server_type = "cx11"
  location = "nbg1"
  ssh_keys = [ hcloud_ssh_key.id_ad.id, hcloud_ssh_key.id_hube.id ]

  network {
    network_id = data.hcloud_network.network-1.id
  }
  firewall_ids = [hcloud_firewall.k8s-firewall.id]
}

resource "local_file" "ansible_inventory" {
  content = templatefile("ansible-inventory.tmpl",
    {
     nodes = hcloud_server.node.*
    }
  )
  filename = "../artifacts/inventory.yaml"
}
