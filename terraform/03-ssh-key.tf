resource "hcloud_ssh_key" "id_ad" {
  name       = "id_ad"
  public_key = file("keys/id_ad.pub")
}
resource "hcloud_ssh_key" "id_hube" {
  name       = "id_hube"
  public_key = file("keys/id_hube.pub")
}
