# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version = "2.1.0"
  hashes = [
    "h1:EYZdckuGU3n6APs97nS2LxZm3dDtGqyM4qaIvsmac8o=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/hetznercloud/hcloud" {
  version     = "1.30.0"
  constraints = "1.30.0"
  hashes = [
    "h1:UOiUjzQsB5c1YbYwQepx0q69s3JUCk9nBgZM7L1uPGg=",
    "zh:02471480d607007cc31a0e787deae1715ba63cdd7ca84b71e3e68fc80f728c61",
    "zh:190c41ba6d2c67f514b54be0f9b3c5c4ee66d03bf96c4682e19f0ff72f72f231",
    "zh:3e6146fe3c33cc16f533faebdaf2714afc0a9bb2d86407f49d5891e7463bf4e3",
    "zh:4203a55ca7892a6547a791aea5371f0c9e2893614c7310d3638227dc84bcee2e",
    "zh:4727f6e75ba9f4056980077e0dc5a0c6c2556786562ce497b17132ea376d3a00",
    "zh:7b78955e60859cec251a3de3a20ac925b7355486586de64e512ab108bded94c0",
    "zh:7da6f473d9010335da6f0ac041b581faf9a2745a39e05d529f729d7e6b520abb",
    "zh:8cdc6cee9038807bc8860297ee55f4c342a3de7b9bae9ac6b6b43e347087e024",
    "zh:8fcdf4eccbaa4e4138f0ed0cc3de2384975bcc4e320b635569cfac91319815ef",
    "zh:932ec403afbcc85e9a7de40c145a150d355e9052102a0b3a1d69cdcd11996604",
    "zh:9b80be65005c11db8b5ec77f016bb903e41a13652ce0affadb3be618627aa52d",
    "zh:9f2b9f217cdb82c3400b5481113d68f1e9ef589be827ffb1f5feb7482a09fa47",
    "zh:a9d69dcaa92597c5884a0a370ac1ff6853bf5dc3c54a7468ea4db2491b7bb8cc",
    "zh:afaef5095b17939457369de34a3d77ded2cc18b5d09da2416cde3688b3a66c02",
  ]
}
